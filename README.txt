Instrucciones para correr el programa:

-Abrir la consola.
-Escribir el comando "java -jar VM.jar" esto si se encuentra ubicado en la direcci�n del archivo, de lo contrario adjuntar la  direcci�n junto con el nombre del archivo.
-El programa fue trabajado en consola, al comando anterior se le debe adjuntar la direcci�n del archivo .vm a convertir, para que este sea enviado como parametro.
-Si la conversi�n fue exitosa se creara el archivo en la misma direcci�n del archivo original, con el mismo nombre pero con la extension  .asm.

NOTA: Se debe de tener cuidado, si en la direcci�n se encuentra un espacio la libreria "File" de Java no la acepta y devolvera un error.
El proyecto fue originalmente trabajado bajo este repositorio, en este se encuentran los diferentes commits.
https://gitlab.com/HSCO2017/Project_VM_Java
