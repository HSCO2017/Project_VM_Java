/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Harry
 */
public class VM {

    public static ArrayList<File> vmFiles;
    public static CodeWriter writer;
    
    public static void main(String[] args) {
        // TODO code application logic here
        try {            
            if (args.length != 1){
            System.out.println("Usage:java VM [filename|directory]");
            }
            else {
                File inputFile = new File(args[0]); 
//                System.out.println("Input file address:");
//                Scanner reader = new Scanner(System.in);
//                String address = reader.next();
//                File inputFile = new File(address);  
                boolean flag = false;
                if (inputFile.isFile()) {
                    if (fileExtension(inputFile)) {
                        vmFiles.add(inputFile);
                        flag = true;
                    }
                    else{
                        System.out.println("The file extension is not .vm.");
                    }
                }
                else if (inputFile.isDirectory()) {
                    vmFiles = getFiles(inputFile);
                    if (vmFiles.isEmpty()) {
                        System.out.println("There are no .vm files in this directory.");
                    }
                }
                
                for (File vmFile : vmFiles) {
                    Parse(vmFile);
                }
                
                writer.close();
                
                if (flag) {
                    System.out.println("The file has been converted.");
                }
                else{
                    System.out.println("The files have been converted.");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }

    public VM() {
        VM.vmFiles = new ArrayList<>();
    }
    
    private static boolean fileExtension(File inputF){
        String outFileName = inputF.getName();
        int indexExtension = outFileName.lastIndexOf(".");
        String outFileNameE = outFileName.substring(indexExtension);
        return outFileNameE.equals(".vm");
    }
    
    public static ArrayList<File> getFiles(File directory){
        File[] files = directory.listFiles();
        ArrayList<File> vmF = new ArrayList<>();
        for (File file : files) {
            if (file.getName().endsWith(".vm")) {
                vmF.add(file);
            }
        }
        return vmF;
    }
    
    public static void Parse(File input) throws IOException{
        Parser parser = new Parser(input);
        writer = new CodeWriter(input);
        while (parser.hasMoreCommands()) {            
            parser.advance();
            String type = parser.commandType();
            if (type.equals("C_ARITHMETIC")) {
                writer.writeArithmetic(parser.arg1());
            }
            else if(type.equals("C_POP")||type.equals("C_PUSH")){
                writer.writePushPop(type, parser.arg1(), parser.arg2());
            }
        }
    }
}
