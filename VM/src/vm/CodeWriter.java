/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Harry
 */
public class CodeWriter {
    private static BufferedWriter writerFile;
    private int arthJumpFlag;
    private static final String arithmetic =
            "@SP\n" +
            "AM=M-1\n" +
            "D=M\n" +
            "A=A-1\n";
       
    public CodeWriter(File input) throws IOException{
        String outFile = input.getAbsolutePath();
        outFile=outFile.replace(".vm", ".asm");

        File outputFile = new File(outFile);
        outputFile.createNewFile(); 
        FileWriter w = new FileWriter(outputFile);
        writerFile = new BufferedWriter(w);
    }
    
    public void writeArithmetic(String command) throws IOException{
        switch(command){
            case "add":
                writerFile.write(arithmetic + "M=M+D\n");
                break;
            case "sub":
                writerFile.write(arithmetic + "M=M-D\n");
                break;
            case "neg":
                writerFile.write("D=0\n"
                        + "@SP\n"
                        + "A=M-1\n"
                        + "M=D-M\n");
                break;
            case "eq":
                writerFile.write(arithmethicJump("JNE"));
                arthJumpFlag++;
                break;
            case "gt":
                writerFile.write(arithmethicJump("JLE"));
                arthJumpFlag++;
                break;
            case "lt":
                writerFile.write(arithmethicJump("JGE"));
                arthJumpFlag++;
                break;
            case "and":
                writerFile.write(arithmetic + "M=M&D\n");
                break;
            case "or":
                writerFile.write(arithmetic + "M=M|D\n");
                break;
            case "not":
                writerFile.write("@SP\n"
                        + "A=M-1\n"
                        + "M=!M\n");
                break;
        }
    }
    
    public void writePushPop(String command, String segment, int index) throws IOException{
        if (command.equals("C_PUSH")) {
            if (segment.equals("constant")){
                writerFile.write("@" + index + "\n"
                        + "D=A\n"
                        + "@SP\n"
                        + "A=M\n"
                        + "M=D\n"
                        + "@SP\n"
                        + "M=M+1\n");
            }
            else if (segment.equals("local")){
                writerFile.write(push("LCL",index,false));
            }
            else if (segment.equals("argument")){
                writerFile.write(push("ARG",index,false));
            }
            else if (segment.equals("this")){
                writerFile.write(push("THIS",index,false));
            }
            else if (segment.equals("that")){
                writerFile.write(push("THAT",index,false));
            }
            else if (segment.equals("temp")){
                writerFile.write("@R5\n" 
                        + "D=A\n"
                        + "@" + index + "\n"
                        + "A=D+A\n"
                        + "D=M\n"
                        + "@SP\n" 
                        + "A=M\n" 
                        + "M=D\n" 
                        + "@SP\n" 
                        + "M=M+1\n");
            }
            else if (segment.equals("pointer") && index == 0){
                writerFile.write(push("THIS",index,true));                
            }
            else if (segment.equals("pointer") && index == 1){
                writerFile.write(push("THAT",index,true));
            }
            else if (segment.equals("static")){
                writerFile.write(push(String.valueOf(16 + index),index,true));
            }
        }
        else if (command.equals("C_POP")) {
            if (segment.equals("local")){
                writerFile.write(pop("LCL",index,false));
            }
            else if (segment.equals("argument")){
                writerFile.write(pop("ARG",index,false));
            }
            else if (segment.equals("this")){
                writerFile.write(pop("THIS",index,false));
            }
            else if (segment.equals("that")){
                writerFile.write(pop("THAT",index,false));
            }
            else if (segment.equals("temp")){
                writerFile.write("@R5\n" 
                        + "D=A\n"
                        + "@" + index + "\n"
                        + "D=D+A\n"
                        + "@R13\n" 
                        + "M=D\n" 
                        + "@SP\n" 
                        + "AM=M-1\n" 
                        + "D=M\n" 
                        + "@R13\n" 
                        + "A=M\n" 
                        + "M=D\n");
            }
            else if (segment.equals("pointer") && index == 0){
                writerFile.write(pop("THIS",index,true));
            }
            else if (segment.equals("pointer") && index == 1){
                writerFile.write(pop("THAT",index,true));
            }
            else if (segment.equals("static")){
                writerFile.write(pop(String.valueOf(16 + index),index,true));
            }
        }
    }
    
    /**
     *
     * @throws IOException
     */
    public void close() throws IOException{
        writerFile.flush();
        writerFile.close();
    }
    
    private String arithmethicJump(String type){
        return "@SP\n" +
                "AM=M-1\n" +
                "D=M\n" +
                "A=A-1\n" +
                "D=M-D\n" +
                "@FALSE" + arthJumpFlag + "\n" +
                "D;" + type + "\n" +
                "@SP\n" +
                "A=M-1\n" +
                "M=-1\n" +
                "@CONTINUE" + arthJumpFlag + "\n" +
                "0;JMP\n" +
                "(FALSE" + arthJumpFlag + ")\n" +
                "@SP\n" +
                "A=M-1\n" +
                "M=0\n" +
                "(CONTINUE" + arthJumpFlag + ")\n";
    }

    private String push(String segment, int index, boolean direct) {
        String noPointer = "";
        if (!direct) {
            noPointer = "@" + index + "\n" 
                    + "A=D+A\n"
                    + "D=M\n";
        }
        return "@" + segment + "\n" 
                + "D=M\n"
                + noPointer 
                + "@SP\n" 
                + "A=M\n" 
                + "M=D\n" 
                + "@SP\n" 
                + "M=M+1\n";
    }
    
    private String pop(String segment, int index, boolean direct) {
        String noPointer = "";
        if (!direct) {
            /*noPointer = "@" + index + "\n" 
                    + "A=D+A\n"
                    + "D=M\n";*/
            noPointer = "D=M\n"
                    + "@" + index + "\n" 
                    + "D=D+A\n";
        }
        else{
            noPointer = "D=A\n";
        }
        return "@" + segment + "\n" 
                + noPointer 
                + "@R13\n" 
                + "M=D\n" 
                + "@SP\n" 
                + "AM=M-1\n" 
                + "D=M\n" 
                + "@R13\n" 
                + "A=M\n" 
                + "M=D\n";
    }
}
