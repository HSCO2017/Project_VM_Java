/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Harry
 */
public class Parser {
    
    private final BufferedReader reader;
    private String current;
    private String next;
    private final ArrayList<String> arithmeticCMD = new ArrayList<>();
    
    public Parser(File input) throws FileNotFoundException, IOException{
        FileReader r = new FileReader(input);
        reader = new BufferedReader(r);
        next=getNextLine();
        fillArithmethicCMD();
    }

    private String getNextLine() throws IOException {
        next = reader.readLine();
        while ((next != null) && (next.isEmpty() || isComment(next))) {
            next = reader.readLine();
            if(next == null){
                return null;
            }
        }
        if(next != null){
            int comment = next.indexOf("//");
            if(comment != -1){
                next = next.substring(0, comment-1);
            }
        }        
        return next;
    }
    
    private boolean isComment(String line){
        return line.trim().startsWith("//");
    }
    
    public boolean hasMoreCommands(){
        return next != null;
    }
    
    public void advance() throws IOException{
        current = next;
        next = getNextLine();
    }
    
    public String commandType(){
        //String line = current.trim();
        if (isArithmethic(current)) {
            return "C_ARITHMETIC";
        }
        else if(current.contains("push")){
            return "C_PUSH";
        }
        else if(current.contains("pop")){
            return "C_POP";
        }
        else if(current.contains("label")){
            return "C_LABEL";
        }
        else if(current.contains("goto")){
            return "C_GOTO";
        }
        else if(current.contains("if")){
            return "C_IF";
        }
        else if(current.contains("function")){
            return "C_FUNCTION";
        }
        else if(current.contains("call")){
            return "C_RETURN";
        }
        else if(current.contains("return")){
            return "C_CALL";
        }
        else{
            return null;
        }        
    }  
        
    public boolean isArithmethic(String line){
        for (int i = 0; i < arithmeticCMD.size(); i++) {
            if(line.contains(arithmeticCMD.get(i))){
                return true;
            }
        }
        return false;
    }
    
    public String arg1(){
        String[] parts = current.split(" ");
        if(commandType().equals("C_ARITHMETIC")){
            return parts[0];
        }else{
            return parts[1];
        }        
    }
    
    public int arg2(){
        String[] parts = current.split(" ");
        return Integer.parseInt(parts[2]);
    }
    
    public final void fillArithmethicCMD(){
        arithmeticCMD.add("add");
        arithmeticCMD.add("sub");
        arithmeticCMD.add("neg");
        arithmeticCMD.add("eq");
        arithmeticCMD.add("gt");
        arithmeticCMD.add("lt");
        arithmeticCMD.add("and");
        arithmeticCMD.add("or");
        arithmeticCMD.add("not");
    }
}
